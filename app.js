const { App } = require('@slack/bolt');
var express = require('express');
const expressApp = express();

const { WebClient } = require('@slack/web-api');

// for parsing the body in POST request
var bodyParser = require('body-parser');

const web = new WebClient(process.env.SLACK_BOT_TOKEN);
expressApp.use(bodyParser.urlencoded({ extended: false }));
expressApp.use(bodyParser.json());
expressApp.post('/message', function (req, res) {
  console.log(req.body.user);
  getSlackUsers(req.body);
  return res.send('Messaged Successfully');
  
});
/**
 * Posts Message to a particular user
 * @param {} requestObj 
 */
async function getSlackUsers(requestObj){
  //Fetch UserID for DM
  let userObj={};
// Given some known conversation ID (representing a public channel, private channel, DM or group DM)
const userName = requestObj.user;
try {
  // Call the users.list method using the WebClient
  const result = await web.users.list();
  console.log(JSON.stringify(result));
  result.members.forEach(function (obj) {
  
  if (obj.name === userName) 
    userObj ={id:obj.id,name:obj.real_name}; 
    return;
  
  });

  }
catch (error) {
  console.error(error);
}
// Post DM to slack
(async () => {

  // Post a message to the channel, and await the result.
  // Find more arguments and details of the response: https://api.slack.com/methods/chat.postMessage
  const result = await web.chat.postMessage({
    text: 'Hello world '+userObj.name,
    channel: userObj.id,
  });

})();

}

expressApp.listen('3000', function(){
  console.log('Server listening on port 3000');
});